# The Codeberg Documentation Contributors

In alphabetic order (by last name / username):

- Martijn de Boer (@sexybiggetje)
- Christian Buhtz (@buhtz)
- Ivan Calandra (@ivan-paleo)
- Ben Cotterell (@benc)
- Lucas Hinderberger (@lhinderberger)
- Henning Jacobs (@hjacobs)
- @mray (for the Codeberg Logo)
- @n
- Alex (@n0542344)
- Holger Waechtler (@hw)
- William Davis (@unbeatable-101)
