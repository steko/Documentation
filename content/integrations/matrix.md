---
eleventyNavigation:
  key: MatrixIntegration
  title: Integrating with Matrix
  parent: Integrations
---

This article will guide you through integrating Codeberg with Matrix, allowing repo updates to be automatically broadcast to your project's Matrix room.

## Create a new Matrix account
To set up the Matrix integration, it is recommended that you [create a new account](https://app.element.io/#/register) dedicated to broadcasting your repo updates. This will act as a bot account. You can create an account on your preferred Matrix server.

## Getting an access token
You will need an access token to give Codeberg access to send updates from your dedicated Matrix account. Here's how to do it on the Element web client.

1. Make sure you are logged in to [Element](https://app.element.io/) with the Matrix account you created earlier in a private/incognito browser window.
2. Click on your account name in the top-left corner, then click on `All Settings` in the dropdown.
3. Optionally, you can set a display name and a profile picture for the account here.
4. Navigate to the `Help & About` tab on the left.
5. Scroll to the bottom and click on latter part of `Access Token: <click to reveal>` in the advanced section.
6. Copy the access token that appears to a safe place.
7. **Do not log out of Element.** This will invalidate your access token. Instead, you can simply close your private/incognito browser.

> Alternatively, you can get an access token directly from the Matrix API. Read more at [Matrix's documentation](https://www.matrix.org/docs/guides/client-server-api#login).

## Connecting your repo to your Matrix using webhooks
Now we will connect the Matrix bot to your Codeberg repository. You will need admin permissions for the repo.

<picture>
  <source srcset="/assets/images/integrations/matrix/webhooks.webp" type="image/webp">
  <img src="/assets/images/integrations/matrix/webhooks.png" alt="webhooks">
</picture>

1. In a web browser, go to your repo and click on the `Settings` button on the top right.
2. Navigate to the `Webhooks` settings tab.
3. Click on the `Add Webhook` button and select `Matrix`.
4. Here are explanations for some key fields: 

  - Homeserver URL: for example `https://matrix.org`
  - Room ID: for example `!VTjWrzxSWgLJnHgDUd:matrix.org`. You can find this on Element in the `Advanced` tab for the room settings.
  - Access token: paste the access token you got earlier.
  - Message type: `m.text` is the most basic type of message which represents text. `m.notice` is intended for bots and [behaves differently](https://matrix.org/docs/spec/client_server/latest#m-notice.).

Add your webhook, then click on the webhook URL. At the bottom now, there will be a `Test Delivery` button. Click this, and you will receive a message on your Matrix room! 