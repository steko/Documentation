---
# This file allows redirecting URLs to prevent link rot.
pagination:
  data: redirects
  size: 1
  alias: redirect
redirects:
  - {"from": "/git/clone-commit-via-ssh/", "to": "/git/clone-commit-via-cli/"}
  - {"from": "/git/clone-commit-via-http/", "to": "/git/clone-commit-via-cli/"}
  - {"from": "/advanced/images-in-wiki-pages/", "to": "/getting-started/wiki/"}
permalink: "{{ redirect.from }}"
layout: redirect
---
