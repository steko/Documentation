---
eleventyNavigation:
  key: Wiki
  title: Integrated Wiki
  parent: GettingStarted
  order: 60
---

A [Wiki](https://en.wikipedia.org/wiki/Wiki) is a collaborative space on the web. It is a common practice to use Wikis to collect knowledge and share information.   
Codeberg allows you to add a Wiki to a repo for additional documentation.

The user in these examples is `knut` the polar bear and its repository is `foobar`.

## Activation and Permissions
To enable the Wiki for a repository, visit the `Settings` page and activate `Enable Repository Wiki` in the `Advanced Section`. It will default to the built-in wiki which is described here, but you can as well add an URI to an external site the "Wiki" tab should link to (not part of this doc).

> **Warning**   
> Be aware that the wiki, once enabled, is accessible for *everyone* who has `read` access your repository - on public repositories even unauthenticated guests can access the wiki.   
> The wiki is *not* a suitable place for storing private information or secrets (like passwords).

To edit the Wiki `write` permission to the repository is required.

## Wiki structure
The Wiki is essentially a separate Git repo in your repository with a predefined name in the form of `<your-repository-name>.wiki.git`.

It consists of [Markdown](https://en.wikipedia.org/wiki/Markdown) files (file extension `.md`) and additional assets like images.   
No further style sheets are needed. The markdown files are automatically rendered according to the selected Codeberg theme.

## Adding content via web 
After you have enabled the Wiki you are asked to create the initial page `Home.md`.

Using the web UI in your browser is currently limited to add/update/delete pages, you can not manage assets like images this way.
<picture>
   <source srcset="/assets/images/getting-started/wiki/wiki_pageview.png" type="image/png">
   <img src="/assets/images/getting-started/wiki/wiki_pageview.png" alt="Wiki home page with edit buttons">
</picture>

## Adding content via a local Git client
You can work with the Wiki repo as with any other Git repo on Codeberg, see our docs about managing a Git repo [via CLI](https://docs.codeberg.org/git/clone-commit-via-cli).

```shell
git clone git@codeberg.org:knut/foobar.wiki.git
cd foobar.wiki
nano Home.md
git commit -am "create Home page"
```

Editing locally allows you to use your favorite editor (preferably with markdown syntax check and highlighting) and manage additional assets like images.

### Adding images
You could add images to the root directory or a specific subfolder (like `assets` or `images`) using your local Git client.

A feasible workflow might look like this:

```shell
# create a subfolder for images
mkdir images
cd images
# copy the image file into this folder
git add images/image.png
git commit -m "add image"
git push
```

Now, you can reference the image in Markdown, like this:

```markdown
![image alt text](images/image.png "image title")
```

After saving your changes, the image should be visible.

> In contrast to embedding external images, images in Git are only rendered after saving the wiki or markdown file changes.
