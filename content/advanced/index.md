---
eleventyNavigation:
  key: AdvancedUsage
  title: Advanced Usage
  icon: terminal 
  order: 65
---

These documentation pages contain tips and tricks for diving more deeply into
the more advanced features of Codeberg.

See also the [documentation of Gitea](https://docs.gitea.io), the software which
Codeberg is based on.
