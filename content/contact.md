---
eleventyNavigation:
  key: Contact
  title: Contact
  icon: envelope
  order: 90
---

## Questions and Issues
The most important place to ask for help and report any issues with Codeberg is the [Codeberg/Community issue tracker](https://codeberg.org/Codeberg/Community/issues). It's never wrong to open a ticket there.

If you need quicker help or want to freely discuss topics, or follow up on Codeberg news, you can
- join the [general Codeberg Matrix Channel](https://matrix.to/#/#codeberg.org:matrix.org) (questions, discussions, chat) or the [Codeberg Documentation Matrix Channel](https://matrix.to/#/%23codeberg-documentation%3Amatrix.org) (questions, documentation related discussion)
- follow us on [Mastodon](https://mastodon.technology/@codeberg) or if you aren't on the free and federated social network yet, [on Twitter](https://twitter.com/codeberg_org). You can also send us a direct message (Mastodon / Fediverse will have quicker response times, though).
- join the unofficial (community-created) `#codeberg` IRC channel at [libera.chat](https://libera.chat)
- use the unofficial (community-created) [Codeberg Subreddit](https://www.reddit.com/r/Codeberg/)

## Email
For direct contact, you can write an email to [contact@codeberg.org](mailto:contact@codeberg.org).

## Legal inquiries
For legal inquiries, please refer to the [Imprint](https://codeberg.org/codeberg/org/src/branch/master/Imprint.md).
